import React from 'react';
// import logo from './logo.svg';
import './App.css';
import './Tugas-15/Theme.css';
// import ProfilData from './Quiz-2/ProfilData';
// import Form from './Tugas-9/Form';
// import Judul from './Tugas-9/Judul';
// import TabelHargaBuah from './Tugas-10/TabelHargaBuah';
// import JudulTabel from './Tugas-10/JudulTabel';
// import Countdown from './Tugas-11/Countdown';
// import ActionForm from './Tugas-12/ActionForm';
// import ActionForm from './Tugas-13/ActionForm';
// import Fruit from './Tugas-14/Fruit'
import Routes from './Tugas-15/Routes'
// import { Router } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      {/* <body className="App-body">
        <Judul/>
        <br />
        <Form />
      </body>
      <br />
      <div className="App">
        <body className="App-body">
          <JudulTabel />
          <TabelHargaBuah />
        </body>
      </div>
      <br />
      <ProfilData />
      <Countdown />  */}
      {/* <ActionForm /> */}
      {/* <Fruit /> */}
      <Routes />
    </div>
  );
}

export default App;
