// import React from 'react';
// import {ThemeProvider} from './ThemeContext';
// // import FruitList from './FruitList';
// import ThemeToggle from './ThemeToggle';

// const ChangeTheme = () => {
//     return(
//         <ThemeProvider>
            
//         </ThemeProvider>
//     )
// }

import React, { useState, useEffect } from "react";
import {ThemeProvider} from "styled-components";
import  {useDarkMode} from "./UseDarkMode"
// import { GlobalStyles } from "./GlobalStyle";
import { lightTheme, darkTheme } from "./Themes";
import Toggle from "./ThemeToggle"


const App = () => {

    const [theme, themeToggler, mountedComponent] = useDarkMode();
    const themeMode = theme === 'light' ? lightTheme : darkTheme;

    if(!mountedComponent) return <div/>
    return(
        <ThemeProvider theme={themeMode}>
            <>
            {/* <GlobalStyles/> */}
            <div className="App">
                <Toggle theme={theme} toggleTheme={themeToggler}/>
            </div>
            </>
        </ThemeProvider>
    )
}

export default App