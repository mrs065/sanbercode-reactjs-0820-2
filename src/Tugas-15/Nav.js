import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default function Nav(){
    return(
        <nav>
            <ul>
                <li>
                    <Link id="menu-item" to="/tugas-9">Tugas 9</Link>
                </li>
                <li>
                    <Link to="/tugas-10">Tugas 10</Link>
                </li>
                <li>
                    <Link to="tugas-11">Tugas 11</Link>
                </li>
                <li>
                    <Link to="tugas-12">Tugas 12</Link>
                </li>
                <li>
                    <Link to="tugas-13">Tugas 13</Link>
                </li>
                <li>
                    <Link to="tugas-14">Tugas 14</Link>
                </li>
                <li>
                    <Link to="tugas-15">Tugas 15</Link>
                </li>
            </ul>
        </nav>
    )
}