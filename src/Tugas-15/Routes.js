import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Nav from "./Nav.js";
// import ProfilData from './Quiz-2/ProfilData';
import Form from '../Tugas-9/Form';
import TabelHargaBuah from '../Tugas-10/TabelHargaBuah';
import Countdown from '../Tugas-11/Countdown';
import ActionForm from '../Tugas-12/ActionForm';
import ActionForm13 from '../Tugas-13/ActionForm';
import Fruit from '../Tugas-14/Fruit';
import ChangeTheme from '../Tugas-15/ChangeTheme';

export default function App(){
    return (
        <Router>
            <div>
                <Nav />
                <Switch>
                    <Route exact path="/tugas-9" component={Form} />
                    <Route exact path="/tugas-10" component={TabelHargaBuah} />
                    <Route exact path="/tugas-11" component={Countdown} />
                    <Route exact path="/tugas-12" component={ActionForm} />
                    <Route exact path="/tugas-13" component={ActionForm13} />
                    <Route exact path="/tugas-14" component={Fruit} />
                    <Route exact path="/tugas-15" component={ChangeTheme} />
                </Switch>
            </div>
        </Router>
    )
}