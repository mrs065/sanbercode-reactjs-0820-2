import React, {useState, createContext} from 'react';

export const ThemeContext = createContext();

export const ThemeProvider = props => {
    const[color, setColor] = useState(null)

    return(
        <ThemeContext.Provider value={[color, setColor]}>
            {props.children}
        </ThemeContext.Provider>
    )
}

