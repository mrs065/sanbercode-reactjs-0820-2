import React, {useContext, useState} from 'react';
import {FruitContext} from './FruitContext'
import axios from 'axios';

const FruitForm = () => {

    const [dataHargaBuah, setDataHargaBuah, input, setInput] = useContext(FruitContext)
    // const [name, setName] = useState("")
    // const [price, setPrice] = useState("")
    // const [weight, setWeight] = useState("")

    const handleChangeNama = (event) => {
        var value = event.target.value
        setInput({...input, name: value})
        // setName(event.target.value)
    }
    
    const handleChangeHarga = (event) => {
        var value = event.target.value
        setInput({...input, price: value})
        // setPrice(event.target.value)
    }
    
    const handleChangeBerat = (event) => {
        var value = event.target.value
        setInput({...input, weight: value})
        // setWeight(event.target.value)
    }

    const handleSubmit= (event) => {
        event.preventDefault()
        var newId = dataHargaBuah.length + 1
        if(input.id === null){
            console.log(input)
            axios.post(` http://backendexample.sanbercloud.com/api/fruits`, { name: input.name, price: input.price, weight: input.weight })
            .then(res => {
                // var data = res.data
                setDataHargaBuah([
                    ...dataHargaBuah, res.data
                ])
                // setName("")
                // setPrice("")
                // setWeight("")
                setInput({
                    id: null,
                    name: "",
                    price: "",
                    weight: ""
                })
            })
        } else {
            axios.put(` http://backendexample.sanbercloud.com/api/fruits/${input.id}`, { name: input.name, price: input.price, weight: input.weight })
            .then(res => {
                var dataHargaBuahBaru = dataHargaBuah.map(x => {
                    if(x.id === input.id){
                        x.name = input.name
                        x.price = input.price
                        x.weight = input.weight
                    }
                    return x
                })
                setDataHargaBuah(dataHargaBuahBaru)
                // setName("")
                // setPrice("")
                // setWeight("")
                setInput({
                    id: null,
                    name: "",
                    price: "",
                    weight: ""
                })
            })
        }
        
    }

    return(
        <>
            <div style={{width: "90%", margin: "0 auto"}}>
                <h1 style={{textAlign: "center"}}>Data Harga Buah</h1>
                <h2>Form Harga Buah</h2>
                <form onSubmit={handleSubmit}>
                    <label>Masukkan nama buah: </label>
                    <input type="text" value={input.name} onChange={handleChangeNama} name="nama" required/>
                    <br />
                    <label>Masukkan harga buah: </label>
                    <input type="text" value={input.price} onChange={handleChangeHarga} name="harga" required/>
                    <br />
                    <label>Masukkan berat buah: </label>
                    <input type="text" value={input.weight} onChange={handleChangeBerat} name="berat" required/>
                    <br />
                    <button>submit</button>
                </form>
            </div>
        </>
    )
}

export default FruitForm