import React, {useContext} from 'react';
import {FruitContext} from './FruitContext';
import axios from 'axios';

const FruitList = () => {
    const [dataHargaBuah, setDataHargaBuah, input, setInput] = useContext(FruitContext)

    // console.log(dataHargaBuah);
    const ubahData = (event) => {
        var idFruit = parseInt(event.target.value)
        var buah = dataHargaBuah.find(x => x.id === idFruit)
        setInput({id: idFruit, name: buah.name, price: buah.price, weight: buah.weight})
    }
    
    const hapusData = (event) => {
        var idFruit = parseInt(event.target.value)
        axios.delete(` http://backendexample.sanbercloud.com/api/fruits/${idFruit}`)
        .then(res => {
            var newDataHargaBuah = dataHargaBuah.filter(x => x.id !== idFruit)
            setDataHargaBuah(newDataHargaBuah)
        })
    }

    return(
        <> 
            <div style={{width: "90%", margin: "0 auto"}}>
                <h2 style={{textAlign: "center"}}>
                    Tabel Harga Buah
                </h2>
                <table style={{border: "1px #000000 solid", cellpadding: "10px", width:"100%", margin:"0 auto"}}>
                    <thead style={{backgroundColor: "#7f7d7c"}}>
                        <tr>
                            <th style={{textAlign: "center"}}>No</th>
                            <th style={{textAlign: "center"}}>Nama</th>
                            <th style={{textAlign: "center"}}>Harga</th>
                            <th style={{textAlign: "center"}}>Berat</th>
                            <th style={{textAlign: "center"}}>Aksi</th>
                        </tr>
                    </thead>
                    <tbody style={{backgroundColor: "#e05a00"}}>
                        {
                            dataHargaBuah !== null && dataHargaBuah.map((item, index) =>{
                                return(
                                    <tr key={item.id}>
                                        <td>{index+1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.price}</td>
                                        <td>{item.weight/1000} kg</td>
                                        <td style={{textAlign: "center"}}>
                                            <button value={item.id} onClick={ubahData}>Edit</button>
                                            &nbsp;
                                            <button value={item.id} onClick={hapusData}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>     
                </table>
            </div>
        </>
    )
}

export default FruitList