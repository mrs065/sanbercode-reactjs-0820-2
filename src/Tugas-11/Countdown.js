import React, {Component} from 'react';

class Countdown extends Component{
    constructor(props){
        super(props)
        this.state = {
            count: 100,
            showTime: true,
            time: new Date().toLocaleTimeString()
        }
    }

    componentDidMount(){
        this.counterID = setInterval(
            () => this.tick(), 1000
        );
    }

    componentDidUpdate(){
        if(this.state.showTime === true){
            if(this.state.count === 0){
                // this.componentWillUnmount()
                this.setState({
                    showTime: false
                })
            }
        }
    }

    componentWillUnmount(){
        clearInterval(this.counterID);
    }

    tick(){
        this.setState({
            count: this.state.count - 1,
            time: new Date().toLocaleTimeString()
        });
    }

    render(){
        return(
            <>
                {this.state.showTime && (
                    <table style={{width: "90%", margin: "0 auto"}}>
                        <tr>
                            <td>
                                <h1 style={{textAlign: "center"}}>
                                    sekarang jam : {this.state.time}
                                </h1>
                            </td>
                            <td>
                                <h1 style={{textAlign: "center"}}>
                                    hitung mundur: {this.state.count}
                                </h1>
                            </td>
                        </tr>
                        
                    </table>
                )} 
            </>
        )
    }
}

export default Countdown