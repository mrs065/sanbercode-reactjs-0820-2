import React from 'react';
import JudulTabel from './JudulTabel';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]

class IsiTabel extends React.Component{
    render(){
        return(
            <tr>
                <td>{this.props.item.nama}</td>
                <td>{this.props.item.harga}</td>
                <td>{this.props.item.berat/1000} kg</td>
            </tr>
        )
    }
}

function GrupIsiTabel(){
    return(
        <>
            {
                dataHargaBuah.map((el)=>{
                    return(
                        <IsiTabel item={el}/>
                    )
                })
            }
        </>
    )
    
}

function TabelHargaBuah(){
    return(
        <div>
            <JudulTabel />
            <table style={{border: "1px #000000 solid", cellpadding: "10px", width:"95%", margin:"0 auto"}}>
                <thead style={{backgroundColor: "#7f7d7c"}}>
                    <tr>
                        <th style={{textAlign: "center"}}>Nama</th>
                        <th style={{textAlign: "center"}}>Harga</th>
                        <th style={{textAlign: "center"}}>Berat</th>
                    </tr>
                </thead>
                <tbody style={{backgroundColor: "#e05a00"}}>
                    <GrupIsiTabel />
                </tbody>
            </table>
        </div>
        
    )
}

export default TabelHargaBuah