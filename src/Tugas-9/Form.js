import React from 'react';
import Judul from './Judul'

class LabelPelanggan extends React.Component{
    render(){
        return(
            <div className="form-label">
                Nama Pelanggan
            </div>
        )
    }
}

class InputPelanggan extends React.Component{
    render(){
        return(
            <input type="text" name="namapelanggan"></input>
        )
    }
}

class LabelItem extends React.Component{
    render(){
        return(
            <div className="form-label">
              Daftar Item
            </div>
        )
    }
}

// let buah = ["Semangka", "Jeruk", "Nanas", "Salak", "Anggur"];

// class ItemBuah extends React.Component {
//     render(){
//         return {this.props.buah}
//     }
// }
// class PilihanBuah extends React.Component{
//     render(){
//         return(
//             {buah.map(buahItem => {
//                 return (
//                     <label><input type="checkbox" name="item" /><ItemBuah buah={buahItem} /></label>
//                 )
//             })}
//         )
//     }
// }

class Semangka extends React.Component{
    render(){
        return <label><input type="checkbox" name="item" value="Semangka" />Semangka</label>
    }
}

class Jeruk extends React.Component{
    render(){
        return <label><input type="checkbox" name="item" value="Jeruk" />Jeruk</label>
    }
}

class Nanas extends React.Component{
    render(){
        return <label><input type="checkbox" name="item" value="Nanas" />Nanas</label>
    }
}

class Salak extends React.Component{
    render(){
        return <label><input type="checkbox" name="item" value="Salak" />Salak</label>
    }
}

class Anggur extends React.Component{
    render(){
        return <label><input type="checkbox" name="item" value="Anggur" />Anggur</label>
    }
}

class TombolKirim extends React.Component{
    render(){
        return(
            <button className="send-button">
                <a href="#">Kirim</a>
            </button>
        )
    }
}


class Form extends React.Component {
    render() {
        return(
            <div>
                <Judul />
                <br />
                <div className="content">
                    <div className="form-group">
                        <LabelPelanggan />
                        <InputPelanggan />
                    </div>
                    <div className="form-group">
                        <LabelItem />
                        <div className="checkbox">
                            <Semangka />
                            <br />
                            <Jeruk />
                            <br />
                            <Nanas />
                            <br />
                            <Salak />
                            <br />
                            <Anggur />
                        </div>
                    </div>
                    <br/>
                    <TombolKirim />
                </div>
            </div>
            
        )
    }
}

export default Form