import React from 'react';
import { render } from '@testing-library/react';

const data = [
    {name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
    {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
    {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
    {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
]


function IsiData(props){
    return(
        <>
            {
                data.map((el)=>{
                    return(
                        <ProfilData item={el}/>
                    )
                })
            }
        </>
    )
}

function ProfilData(props){
        return(
            <div className="App-body">
                <img src={this.props.item.photo}></img>
                <br />
                <h3>{this.props.item.name}</h3>
                <br />
                <p>{this.props.item.profession}</p>
                <br />
                <p>{this.props.item.age} years old</p>
            </div>
        )
}

export default ProfilData