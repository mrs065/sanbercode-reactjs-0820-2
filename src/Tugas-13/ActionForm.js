import React, {useState, useEffect} from 'react';
import axios from 'axios';

const ActionForm = () => {
    const[dataHargaBuah, setDataHargaBuah] = useState(null)
    const[input, setInput] = useState({
        id: null,
        name: "",
        price: "",
        weight: ""
    })
    
    useEffect(() => {
        if(dataHargaBuah === null){
            axios.get("http://backendexample.sanbercloud.com/api/fruits")
            .then(res => {
                setDataHargaBuah(res.data)
                console.log(res.data)
            })
        }
    }, [dataHargaBuah]);

    const handleChangeNama = (event) => {
        var value = event.target.value
        setInput({...input, name: value})
    }
    
    const handleChangeHarga = (event) => {
        var value = event.target.value
        setInput({...input, price: value})
    }
    
    const handleChangeBerat = (event) => {
        var value = event.target.value
        setInput({...input, weight: value})
    }

    // const handleChange = (event) => {
    //     let typeOfInput = event.target.value
    //     switch (typeOfInput){
    //         case "name":
    //         {
    //             setInput({...input, name: typeOfInput});
    //             break
    //         }
    //         case "harga":
    //         {
    //             setInput({...input, price: typeOfInput});
    //             break
    //         }
    //         case "berat":
    //         {
    //             setInput({...input, weight: typeOfInput})
    //             break
    //         }
    //         default:
    //         {break;}
    //     }
    // }

    const handleSubmit= (event) => {
        event.preventDefault()
        if(input.id === null){
            axios.post(` http://backendexample.sanbercloud.com/api/fruits`, { name: input.name, price: input.price, weight: input.weight })
            .then(res => {
                var data = res.data
                setDataHargaBuah([
                    ...dataHargaBuah,
                    {
                        id: data.id,
                        name: data.name,
                        price: data.price,
                        weight: data.weight
                    }
                ])
                setInput({
                    id: null,
                    name: "",
                    price: "",
                    weight: ""
                })
            })
        } else {
            axios.put(` http://backendexample.sanbercloud.com/api/fruits/${input.id}`, { name: input.name, price: input.price, weight: input.weight })
            .then(res => {
                var dataHargaBuahBaru = dataHargaBuah.map(x => {
                    if(x.id === input.id){
                        x.name = input.name
                        x.price = input.price
                        x.weight = input.weight
                    }
                    return x
                })
                setDataHargaBuah(dataHargaBuahBaru)
                setInput({
                    id: null,
                    name: "",
                    price: "",
                    weight: ""
                })
            })
        }
        
    }
    
    const ubahData = (event) => {
        var idFruit = parseInt(event.target.value)
        var buah = dataHargaBuah.find(x => x.id === idFruit)
        setInput({id: idFruit, name: buah.name, price: buah.price, weight: buah.weight})
    }
    
    const hapusData = (event) => {
        var idFruit = parseInt(event.target.value)
        axios.delete(` http://backendexample.sanbercloud.com/api/fruits/${idFruit}`)
        .then(res => {
            var newDataHargaBuah = dataHargaBuah.filter(x => x.id !== idFruit)
            setDataHargaBuah(newDataHargaBuah)
        })
    }

    
    return(
        <>
            <div style={{width: "90%", margin: "0 auto"}}>
                <h1 style={{textAlign: "center"}}>Data Harga Buah</h1>
                <h2>Form Harga Buah</h2>
                <form onSubmit={handleSubmit}>
                    <label>Masukkan nama buah: </label>
                    <input type="text" value={input.name} onChange={handleChangeNama} name="nama" required/>
                    <br />
                    <label>Masukkan harga buah: </label>
                    <input type="text" value={input.price} onChange={handleChangeHarga} name="harga" required/>
                    <br />
                    <label>Masukkan berat buah: </label>
                    <input type="text" value={input.weight} onChange={handleChangeBerat} name="berat" required/>
                    <br />
                    <button>submit</button>
                </form>
                <h2 style={{textAlign: "center"}}>
                    Tabel Harga Buah
                </h2>
                <table style={{border: "1px #000000 solid", cellpadding: "10px", width:"100%", margin:"0 auto"}}>
                    <thead style={{backgroundColor: "#7f7d7c"}}>
                        <tr>
                            <th style={{textAlign: "center"}}>No</th>
                            <th style={{textAlign: "center"}}>Nama</th>
                            <th style={{textAlign: "center"}}>Harga</th>
                            <th style={{textAlign: "center"}}>Berat</th>
                            <th style={{textAlign: "center"}}>Aksi</th>
                        </tr>
                    </thead>
                    <tbody style={{backgroundColor: "#e05a00"}}>
                        {
                            dataHargaBuah !== null && dataHargaBuah.map((item, index)=>{
                                return(
                                    <tr key={item.id}>
                                        <td>{index+1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.price}</td>
                                        <td>{item.weight/1000} kg</td>
                                        <td style={{textAlign: "center"}}>
                                            <button value={item.id} onClick={ubahData}>Edit</button>
                                            &nbsp;
                                            <button value={item.id} onClick={hapusData}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>     
                </table>
            </div>
        </>
    )
    
}

export default ActionForm