import React, {Component} from 'react';

class ActionForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            dataHargaBuah : [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500}
            ],
            inputNama: "",
            inputHarga: "",
            inputBerat: "",
            index: -1
        }

        this.handleChangeNama = this.handleChangeNama.bind(this);
        this.handleChangeHarga = this.handleChangeHarga.bind(this);
        this.handleChangeBerat = this.handleChangeBerat.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    // handleChange(event){
    //     this.setState({
    //         ...state,
    //         [inputNama, inputHarga, inputBerat]: event.target.value
    //         // inputHarga: event.target.value,
    //         // inputBerat: event.target.value
    //     })
    // }

    handleChangeNama(event){
        this.setState(
            {inputNama: event.target.value}
        )
    }

    handleChangeHarga(event){
        this.setState(
            {inputHarga: event.target.value}
        )
    }

    handleChangeBerat(event){
        this.setState(
            {inputBerat: event.target.value}
        )
    }

    handleSubmit(event){
        event.preventDefault()
        console.log(this.state.inputNama)
        console.log(this.state.inputHarga)
        console.log(this.state.inputBerat)
        var index = this.state.index
        if(index === -1){
            this.setState({
                dataHargaBuah: [
                    ...this.state.dataHargaBuah,
                    {
                        nama: this.state.inputNama,
                        harga: this.state.inputHarga,
                        berat: this.state.inputBerat
                    }
                ],
                inputNama: "",
                inputHarga: "",
                inputBerat: ""
            })
        } else {
            var buahBaru = this.state.dataHargaBuah
            buahBaru[index].nama = this.state.inputNama
            buahBaru[index].harga = this.state.inputHarga
            buahBaru[index].berat = this.state.inputBerat

            this.setState({
                dataHargaBuah: [...buahBaru],
                inputNama: "",
                inputHarga: "",
                inputBerat: "",
                index: -1
            }) 
        }
        
    }

    ubahData = (event) => {
        var index = event.target.value
        var buah = this.state.dataHargaBuah[index]
        this.setState({
            inputNama: buah.nama,
            inputHarga: buah.harga,
            inputBerat: buah.berat,
            index
        })
    }

    hapusData = (event) => {
        var index = event.target.value
        var dataBuahBaru = this.state.dataHargaBuah
        dataBuahBaru.splice(index, 1)
        this.setState({
            dataHargaBuah: [...dataBuahBaru],
            inputNama: "",
            inputHarga: "",
            inputBerat: "",
            index: -1
        })
    }

    render(){
        return(
            <>
                <div style={{width: "90%", margin: "0 auto"}}>
                    <h1 style={{textAlign: "center"}}>Data Harga Buah</h1>
                    <h2>Form Harga Buah</h2>
                    <form onSubmit={this.handleSubmit}>
                        <label>Masukkan nama buah: </label>
                        <input type="text" value={this.state.inputNama} onChange={this.handleChangeNama} required/>
                        <br />
                        <label>Masukkan harga buah: </label>
                        <input type="text" value={this.state.inputHarga} onChange={this.handleChangeHarga} required/>
                        <br />
                        <label>Masukkan berat buah: </label>
                        <input type="text" value={this.state.inputBerat} onChange={this.handleChangeBerat} required/>
                        <br />
                        <button>submit</button>
                    </form>
                    <h2 style={{textAlign: "center"}}>
                        Tabel Harga Buah
                    </h2>
                    <table style={{border: "1px #000000 solid", cellpadding: "10px", width:"100%", margin:"0 auto"}}>
                        <thead style={{backgroundColor: "#7f7d7c"}}>
                            <tr>
                                <th style={{textAlign: "center"}}>No</th>
                                <th style={{textAlign: "center"}}>Nama</th>
                                <th style={{textAlign: "center"}}>Harga</th>
                                <th style={{textAlign: "center"}}>Berat</th>
                                <th style={{textAlign: "center"}}>Aksi</th>
                            </tr>
                        </thead>
                        <tbody style={{backgroundColor: "#e05a00"}}>
                            {
                                this.state.dataHargaBuah.map((val, index)=>{
                                    return(
                                        <tr key={index}>
                                            <td>{index+1}</td>
                                            <td>{val.nama}</td>
                                            <td>{val.harga}</td>
                                            <td>{val.berat/1000} kg</td>
                                            <td style={{textAlign: "center"}}>
                                                <button value={index} onClick={this.ubahData}>Edit</button>
                                                &nbsp;
                                                <button value={index} onClick={this.hapusData}>Delete</button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>     
                    </table>
                </div>
            </>
        )
    }
}

export default ActionForm